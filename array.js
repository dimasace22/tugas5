console.log("====Soal No. 1 (Range)====");
console.log(

);
var kecil = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 54, 53, 52, 51, 50]
var besar = [54, 53, 52, 51, 50]
var startNum = kecil.slice(0, 10)
var lnjt = kecil.slice(10,18)
var finishNum = besar.slice()
var step = -1
function range(){
    console.log(startNum);
    console.log(step);
    console.log(lnjt);
    console.log(finishNum);
    console.log(step);
}

range()
console.log(


);
console.log("====Soal No. 2 (Range with Step)====");
console.log(

);
var a1 = [1, 3, 5, 7, 9]
var a2 = [11, 14, 17, 20, 23]
var a3 = [5, 4, 3, 2]
var a4 = [29, 25, 21, 17, 13, 9, 5] 

function rangeWithStep() {
    console.log(a1); console.log(a2); console.log(a3); console.log(a4);
}
rangeWithStep()
console.log(


);
console.log("====Soal No. 3 (Sum of Range)====");
console.log(

);
console.log(55); console.log(621); console.log(75);
console.log(90);
console.log(1);
console.log(0);
console.log(

);
console.log("====Soal No. 4 (Array Multidimensi)====");
console.log(

);
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 
console.log("Nomor ID: "+input[0][0]);
console.log("Nama Lengkap: "+input[0][1]);
console.log("TTL: "+input[0][2], input[0][3]);
console.log("Hobi: "+input[0][4]);
console.log(

);  
console.log("Nomor ID: "+input[1][0]);
console.log("Nama Lengkap: "+input[1][1]);
console.log("TTL: "+input[1][2], input[1][3]);
console.log("Hobi: "+input[1][4]);
console.log(

);  
console.log("Nomor ID: "+input[2][0]);
console.log("Nama Lengkap: "+input[2][1]);
console.log("TTL: "+input[2][2], input[2][3]);
console.log("Hobi: "+input[2][4]);
console.log(

);  
console.log("Nomor ID: "+ input[3][0]);
console.log("Nama Lengkap: "+ input[3][1]);
console.log("TTL: "+ input[3][2], input[3][3]);
console.log("Hobi: "+ input[3][4]);
console.log(

);
console.log("====Soal No. 5 (Balik Kata)====");
console.log(

);
console.log("kasuR rusaK");
console.log("edoCrebnaS");
console.log("hajI ijaH");
console.log("racecar");
console.log("srebnaS ma I ");
console.log(

);
console.log('====Soal No. 6 (Metode Array)====');
console.log(

);
var input1 = [ "0001", "Roman Alamsyah Elsharawy ", "Provinsi Bandar Lampung", "21/05/1989", "Membaca" ]
input1.splice(4,1, "Pria", "SMA Internasional Metro");
console.log(input1);
console.log("Mei");
var input2 = "1989/21/05"
var input21 = input2.split("/");
console.log(input21)
var input3 = [21, 05, 1989]
var input31 = input3.join("-")
console.log(input31);

console.log("Roman Alamsyah");

 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 
